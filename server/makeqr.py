import qrcode
from qrcode.constants import ERROR_CORRECT_H
import qrcode.image.pil

from PIL import Image

from os.path import join, dirname

logo_size = 80

def make_qr_code(data):
    qr = qrcode.QRCode(ERROR_CORRECT_H)
    qr.add_data(data)

    qr_image: Image.Image = qr.make_image().convert("RGBA")

    fitcoin_overlay = Image.open(join(dirname(__file__), "fitcoin_bw.png")).convert("RGBA")
    fitcoin_overlay = fitcoin_overlay.resize((logo_size, logo_size), resample=Image.ANTIALIAS)

    x = int(qr_image.size[0] / 2 - logo_size / 2)
    y = int(qr_image.size[1] / 2 - logo_size / 2)
    qr_image.paste(fitcoin_overlay, (x,y), fitcoin_overlay)

    return qr_image