import os

from flask import Flask, request
from server.database import mongo

app = Flask(__name__, instance_relative_config=True)
app.config["MONGO_URI"] = "mongodb://localhost:27017/fitcoin"
mongo.init_app(app)

from server.services import services_bp
from server.users import users_bp

app.register_blueprint(services_bp)
app.register_blueprint(users_bp)