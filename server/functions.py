from datetime import datetime, timezone
def to_utc_string(obj):
    return obj.replace(tzinfo=timezone.utc).astimezone(tz=None).strftime("%Y-%m-%dT%H:%M:%SZ")