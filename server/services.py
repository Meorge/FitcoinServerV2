from os import link
from io import BytesIO
from re import L
from server.makeqr import make_qr_code
from flask import Blueprint, request, Response
from secrets import token_urlsafe
from datetime import datetime

from pymongo.collection import ReturnDocument
from pymongo.message import update
from server.database import mongo
from server.users import authenticate_user
from bson.objectid import ObjectId
from bson.errors import InvalidId

services_bp = Blueprint('service', __name__)

@services_bp.route('/api/service/register', methods=["POST"])
def api_register_service():
    """
    Registers a new service that belongs to the provided user

    Params:
    - identifier: a string that should identify the service internally
    - display_name: the name of the service, suitable for display to the user
    """

    auth_result = authenticate_user(request)
    if auth_result[0] == False: return { "message": auth_result[1] }, 400

    user = auth_result[2]

    print(request.form)

    # Ensure identifier/display name were provided and they're strings
    identifier = request.form.get("identifier")
    display_name = request.form.get("display_name")

    if identifier is None:
        return { "message" : "Identifier not specified" }, 400

    if display_name is None:
        return { "message" : "Display name not specified" }, 400

    if not isinstance(identifier, str):
        return { "message" : "Identifier must be a string" }, 400

    if not isinstance(display_name, str):
        return { "message" : "Display name must be a string" }, 400


    # Ensure identifier is unique (doesn't already exist)
    if mongo.db.services.count_documents({ "identifier" : identifier }, limit = 1) > 0:
        return { "message" : "Identifier is already in use" }, 400

    # Create access token
    access_token = token_urlsafe(32)

    # Create the new service object
    new_service = {
        "owner_id": user["_id"],
        "identifier": identifier,
        "display_name": display_name,
        "access_token": access_token,
        "creation_date" : datetime.utcnow()
    }

    result = mongo.db.services.insert_one(new_service)

    mongo.db.users.find_one_and_update(
        { "_id" : user["_id"] },
        { "$push" : { "owned_services" : result.inserted_id } }
    )

    return {
        "message" : "Success",
        "access_token" : access_token
    }


@services_bp.route('/api/service/set', methods=["POST"])
def api_set_service_info():
    """
    Sets the display name and/or identifier of a service. The access
    token must be passed in AND the owner's credentials must be sent
    in the Authorization header.
    
    Params:
    - access_token: the access token for the service
    - display_name (optional): the new name for the service
    - identifier (optional): the new identifier for the service
    """
    success, message, user = authenticate_user(request)
    if success == False: return { "message" : message }, 400

    
    access_token = request.form.get("access_token")

    if access_token is None:
        return { "message" : "Access token not provided" }, 400

    # Find the service that belongs to this user, and has this access token
    service = mongo.db.services.find_one({"access_token": access_token, "owner_id": user["_id"]})

    if service is None:
        return { "message" : "Service not found" }, 400

    # Figure out what we want to set for the service
    new_display_name = request.form.get("display_name", type=str)
    new_identifier = request.form.get("identifier", type=str)

    new_values = {}
    if new_display_name is not None: new_values["display_name"] = new_display_name
    if new_identifier is not None: new_values["identifier"] = new_identifier

    if len(new_values) == 0:
        return { "message" : "Neither display_name nor identifier were specified" }, 400

    # Service was found, so let's update the name for it
    updated_service = mongo.db.services.find_one_and_update(
        {"_id": service["_id"]},
        {
            "$set" : new_values
        }
    )

    if updated_service is None:
        return { "message" : "Could not update values" }, 400

    return { "message" : "Success", "new_values" : new_values }, 200


@services_bp.route('/api/service/link/create', methods=["POST"])
def api_create_link_request():
    """
    Creates a new link request for a service

    Params:
    - access_token: the access token for the service
    """

    access_token = request.form.get("access_token", type=str)
    if access_token is None:
        return { "message" : "Access token not provided" }, 400


    # Find the associated identifier
    service = mongo.db.services.find_one({ "access_token" : access_token })

    if service is None:
        return { "message" : "Service not found" }, 404

    # So we have an access token, now let's create an empty link request
    # for it.
    new_link_request = {
        "service_id" : service["_id"],
        "creation_date" : datetime.utcnow(),
        "user_id" : None,
        "status" : "pending"
    }


    # Add this link request to the list of active requests
    result = mongo.db.link_requests.insert_one(new_link_request)

    if result is None:
        return { "message" : "Could not create link request" }, 400

    return { "message" : "Success", "data" : str(result.inserted_id) }, 200


def get_link_request_from_id(request):
    link_request_id = request.args.get("link_request_id", type=str)
    if link_request_id is None:
        print("no link request ID")
        return (False, "Link request ID not provided", None)

    try:
        link_request_id_as_objectid = ObjectId(link_request_id)
    except InvalidId:
        return (False, "Invalid link request ID provided", None)

    # Find the link request
    link_request = mongo.db.link_requests.find_one(link_request_id_as_objectid)

    if link_request is None:
        return (False, "Link request not found", None)


    return (True, "Success", link_request)

@services_bp.route('/api/service/link/status', methods=["GET"])
def api_get_link_status():
    """
    Returns the current status of the given link request

    Params:
    - link_request_id: the ID of the link request (returned from /service/link/create)
    """

    success, message, link_request = get_link_request_from_id(request)

    if not success:
        return { "message" : message }, 400
    
    return { 
        "message" : "Success",
        "data" : {
            "creation_date" : link_request["creation_date"].isoformat(),
            "user_id" : str(link_request["user_id"]),
            "status" : link_request["status"]
        }
        }, 200


@services_bp.route('/api/service/link/about', methods=["GET"])
def api_get_user_info_for_link():
    """
    Returns user-friendly information about the service this link
    request is for.

    Params:
    - link_request_id: the ID of the link request
    """
    success, message, link_request = get_link_request_from_id(request)
    if not success: return { "message" : message }, 400

    # Get the human-readable service name
    try:
        service_id = ObjectId(link_request["service_id"])
    except InvalidId:
        return { "message" : "Service ID associated with this link request is invalid" }, 500
    except KeyError:
        return { "message" : "Link request does not have a service ID" }, 500

    service = mongo.db.services.find_one(service_id)

    if service is None:
        return { "message" : "Service associated with this link request could not be found" }, 500

    if "display_name" not in service:
        return { "message" : "Service does not contain a display name" }, 500

    return {
        "message" : "Success",
        "data" : { 
            "service_name" : service["display_name"],
            "service_id" : str(link_request["service_id"]),
            "link_request_id" : str(link_request["_id"])
            }
    }, 200


@services_bp.route('/api/service/link/respond', methods=["POST"])
def api_respond_to_link_request():
    """
    Either approves or rejects the link request. If approving, the user's credentials
    must be sent in the Authorization header.

    Params:
    - link_request_id: the ID of the link request
    - action: either "approve" or "deny"
    """
    success, message, link_request = get_link_request_from_id(request)
    if not success: return { "message" : message }, 400

    user_action_str = request.args.get("action", type=str)
    if user_action_str is None:
        return { "message" : "Action not specified" }, 400
    

    if user_action_str == "approve":
        # Authenticate user
        success, message, user = authenticate_user(request)
        if success == False: return { "message" : message }, 400

        # Get user's ID, set it to the link request
        user_id = user["_id"]


        link_item = {
            "service_id" : link_request["service_id"],
            "date" : datetime.utcnow()
        }

        updated_user = mongo.db.users.update_one(
            { "_id" : user_id },
            { "$push" : { "approved_services" : link_item } }
        )

        if updated_user is None:
            return { "message" : "Could not update user" }, 500
            
        updated_link_request = mongo.db.link_requests.find_one_and_update(
            { "_id" : link_request["_id"] },
            { "$set" : { "status" : "approved", "user_id" : user_id } }
        )

        

    elif user_action_str == "deny":
        # No need to authenticate user
        # Set link request to denied
        updated_link_request = mongo.db.link_requests.find_one_and_update(
            { "_id" : link_request["_id"] },
            { "$set" : { "status" : "denied" } }
        )

    else: return { "message" : "Action must be either 'approve' or 'deny'" }, 400

    if updated_link_request is None:
        return { "message" : "Could not update link request" }, 500

    return { "message" : "Success" }, 200


@services_bp.route('/api/service/link/clear', methods=["POST"])
def api_clear_link_request():
    """
    Deletes the given link request.

    Params:
    - link_request_id: the ID of the link request
    """
    success, message, link_request = get_link_request_from_id(request)
    if not success: return { "message" : message }, 400

    # Delete link request
    deleted = mongo.db.link_requests.delete_one({"_id" : link_request["_id"] })

    if deleted.deleted_count == 0:
        return { "message" : "Could not delete link request" }, 500

    return { "message" : "Success" }, 200

@services_bp.route('/api/service/user_info', methods=["GET"])
def api_user_info():
    """
    If allowed, get user info

    Params:
    - access_token: the ID for the service
    - user_id: the ID for the user (must have approved the service)
    """
    # TODO: This is copied from api_make_purchase; see if you can condense it
    # into one function

    # First, let's get the user
    try:
        user_id = ObjectId(request.args["user_id"])
    except InvalidId:
        print("Invalid user ID")
        return { "message" : "User ID is invalid" }, 400
    except KeyError:
        print("user ID not provided")
        return { "message" : "User ID not provided" }, 400

    user = mongo.db.users.find_one(user_id)

    if user is None:
        print("user with ID doesnt exist")
        return { "message" : "User with ID does not exist" }, 400


    # Next, let's get the service ID
    access_token = request.args.get("access_token")
    if access_token is None:
        print("no access token provided")
        return { "message" : "Service access token not provided" }, 400

    service = mongo.db.services.find_one({ "access_token" : access_token })
    if service is None:
        print("service with access token not found")
        return { "message" : "Service with access token does not exist" }, 400

    service_id = service["_id"]

    # Now, we'll check if the user has this service in their list of approved services.
    approved_services = user["approved_services"]

    ###
    ###
    ###
    # TODO: Update this to use the new data structure (service_id and date)!!!
    ###
    ###
    ###
    if not any(user_service["service_id"] == service_id for user_service in approved_services):
        print("service not approved by user")
        return { "message" : "Service is not approved by user" }, 400

    # Return the info allowed:
    info = {
        "username" : user["username"],
        "balance" : user["balance"]
    }

    return { "message" : "Success", "data" : info }, 200

@services_bp.route('/api/service/purchase', methods=["POST"])
def api_make_purchase():
    """
    Attempts to withdraw the given number of Fitcoin from the account with
    the provided ID. If the account has not authorized the service to make
    withdrawals, this will return an error.

    Params:
    - access_token: the access token for the service
    - user_id: the ID for the user to make the purchase for
    - amount: the number of Fitcoin to withdraw from the user's account
    """

    # First, let's get the user
    try:
        user_id = ObjectId(request.args["user_id"])
    except InvalidId:
        return { "message" : "User ID is invalid" }, 400
    except KeyError:
        return { "message" : "User ID not provided" }, 400

    user = mongo.db.users.find_one(user_id)

    if user is None:
        return { "message" : "User with ID does not exist" }, 400


    # Next, let's get the service ID
    access_token = request.args.get("access_token")
    if access_token is None:
        return { "message" : "Service access token not provided" }, 400

    service = mongo.db.services.find_one({ "access_token" : access_token })
    if service is None:
        return { "message" : "Service with access token does not exist" }, 400

    service_id = service["_id"]

    # Now, we'll check if the user has this service in their list of approved services.
    approved_services = user["approved_services"]
    if not any(user_service["service_id"] == service_id for user_service in approved_services):
        return { "message" : "Service is not approved by user" }, 400

    # Ensure that amount to remove was valid
    try:
        to_remove = request.args.get("amount", type=int)
    except ValueError:
        return { "message" : "Amount to remove must be an integer" }, 400

    if to_remove is None:
        return { "message" : "Amount to remove not specified" }, 400
    
    if to_remove < 0:
        return { "message" : "Amount to remove must be positive" }, 400


    # Ensure that the user has sufficient funds
    funds_in_account = user["balance"]
    if to_remove > funds_in_account:
        return { "message" : "User has insufficient funds" }, 400

    # All good to go! Remove this amount from the user's account
    new_transaction = {
        "type" : "purchase",
        "amount" : to_remove,
        "service_id" : service_id,
        "date" : datetime.utcnow()
    }
    user_new = mongo.db.users.find_one_and_update(
        { "_id" : user_id },
        {
            "$inc" : { "balance" : -to_remove },
            "$push": { "transactions" : new_transaction }
        },
        return_document=ReturnDocument.AFTER
        )

    if user_new is None:
        return { "message" : "Could not remove funds from user" }, 500

    

    return { "message" : "Success", "data" : user_new["balance"] }, 200


@services_bp.route('/api/service/link/qr', methods=["GET"])
def get_qr_code():
    """
    Returns a QR code corresponding to a given link request.

    Params:
    - link_request_id: the ID of the link request
    """

    # Ensure that this is a valid link request
    success, message, link_request = get_link_request_from_id(request)
    if not success: return { "message" : message }, 400

    qr_code_data = f"LR_{str(link_request['_id'])}"

    buffer = BytesIO()
    qr_code = make_qr_code(qr_code_data)
    qr_code.save(buffer, format="PNG")
    return Response(buffer.getvalue(), mimetype="image/png"), 200