from flask import Blueprint, request
from passlib.hash import pbkdf2_sha256
from secrets import token_urlsafe
from datetime import datetime
from server.database import mongo
from pymongo import ReturnDocument
from server.functions import to_utc_string
from bson.objectid import ObjectId
from bson.errors import InvalidId

users_bp = Blueprint('user', __name__)

@users_bp.route('/api/user/create', methods=["POST"])
def api_create_account():
    """
    Takes a username and password in the Authorization header
    and creates an account, then returns an access token
    """

    if request.authorization is None:
        return { "message" : "Authorization header not included" }, 400

    username = request.authorization.username
    password = request.authorization.password

    # Determine if this username already exists in the database
    if mongo.db.users.count_documents({ "username": username }, limit = 1) > 0:
        return { "message": "User with that username already exists" }, 400

    # Use PassLib to hash the password
    hashed_password = pbkdf2_sha256.hash(password)

    # Generate an access token for this user
    new_access_token = token_urlsafe(32)

    # Create a new entry in the database
    mongo.db.users.insert_one({
        "username": username,
        "password": hashed_password,
        "balance": 0,
        "last_deposit": datetime.utcnow(),
        "access_tokens": [
            new_access_token
        ],
        "transactions": [],
        "approved_services": [],
        "owned_services": []
    })

    # Return the access token
    return {
        "message": "User account created",
        "username": username,
        "access_token": new_access_token
    }

def authenticate_user(request):
    if request.authorization is None:
        return (False, "Authorization header not included", None)

    username = request.authorization.username
    password = request.authorization.password

    user = mongo.db.users.find_one({"username": username})

    if user is None:
        return (False, "User not found", None)

    # If we've gotten here, there's a user with this username
    # Let's see if their password matches
    password_is_correct = pbkdf2_sha256.verify(password, user["password"])

    print(f"Attempted to log in with account {username}, success = {password_is_correct}")

    if not password_is_correct:
        return (False, "Invalid password", None)

    return (True, "", user)


@users_bp.route('/api/user/balance', methods=["GET"])
def api_balance():
    """
    Logs in to an existing account and returns the current balance
    """

    auth_result = authenticate_user(request)

    if auth_result[0] == False:
        return { "message": auth_result[1] }, 400

    user = auth_result[2]

    # If we've gotten here, we've authenticated the user
    # Let's get their balance and return it!
    balance = user["balance"] if "balance" in user else 0
    last_deposit_date = user["last_deposit"]
    return { "message" : "Success", "data": { "balance" : balance, "last_deposit" : to_utc_string(last_deposit_date) }}, 200


@users_bp.route('/api/user/deposit', methods=["POST"])
def api_deposit():
    """
    Logs in to an existing account and deposits a given amount
    """

    auth_result = authenticate_user(request)

    if auth_result[0] == False:
        return { "message": auth_result[1] }, 400

    user_id = auth_result[2]["_id"]

    # Ensure that the amount to add was specified
    try:
        to_add = request.args.get("amount", type=int)
    except ValueError:
        return { "message" : "Amount to add must be an integer" }, 400
        
    if to_add is None:
        return { "message" : "Amount to add not specified" }, 400

    # Ensure that the amount to add was positive
    if to_add < 0:
        return { "message" : "Amount to add must be positive" }, 400

    # Create new transaction object
    new_transaction = {
        "type": "deposit",
        "amount": to_add,
        "date": datetime.utcnow()
    }

    # Update the user with the new balance
    new_user = mongo.db.users.find_one_and_update(
        {"_id": user_id},
        {
            "$inc": {"balance": to_add},
            "$push": {"transactions": new_transaction},
            "$set": {"last_deposit": datetime.utcnow()}
        },
        return_document=ReturnDocument.AFTER)

    return { "message": "Success", "data": new_user["balance"] }, 200


@users_bp.route('/api/user/owned_services', methods=["GET"])
def api_get_owned_services():
    """
    Returns the services owned by a user
    """
    auth_result = authenticate_user(request)

    if auth_result[0] == False:
        return { "message": auth_result[1] }, 400

    service_ids = auth_result[2]["owned_services"]


    services = list(mongo.db.services.find({"_id": {"$in": service_ids } }))

    for service in services:
        del service["_id"]
        del service["owner_id"]
        service["creation_date"] = to_utc_string(service["creation_date"])

    print(services)
    return { "message" : "Success", "data": services }, 200


@users_bp.route('/api/user/approved_services', methods=["GET"])
def api_get_approved_services():
    """
    Returns the services approved by a user
    """
    success, message, user = authenticate_user(request)
    if success == False: return { "message" : message }, 400

    links = user["approved_services"]

    service_items = []

    for link in links:
        service = mongo.db.services.find_one( { "_id" : link["service_id"] })
        new_item = {
            "date" : to_utc_string(link["date"]),
            "service_id" : str(link["service_id"]),
            "service_name" : service["display_name"] if service is not None else "Unknown Service"
        }
        service_items.append(new_item)

    return { "message" : "Success", "data" : service_items }, 200


@users_bp.route('/api/user/transactions', methods=["GET"])
def api_get_transactions():
    """
    Returns a user's transaction history
    """
    success, message, user = authenticate_user(request)
    if success == False: return { "message" : message }, 400

    transactions = user["transactions"]

    for t in transactions:
        t["date"] = to_utc_string(t["date"])

        if "service_id" in t:
            # add service name
            service = mongo.db.services.find_one( {"_id" : t["service_id"] })
            t["service_name"] = service["display_name"] if service is not None else "Unknown Service"

            #stringify service id
            t["service_id"] = str(t["service_id"])

    transactions = sorted(transactions, key=lambda t: t["date"], reverse=True)

    return { "message" : "Success", "data" : transactions }, 200


@users_bp.route('/api/user/last_deposit', methods=["GET"])
def api_last_deposit():
    """
    Returns the date of the user's last deposit
    """
    success, message, user = authenticate_user(request)
    if success == False: return { "message" : message }, 400

    last_deposit_date = user["last_deposit"]

    return { "message" : "Success", "data" : last_deposit_date }, 200

@users_bp.route('/api/user/remove_service', methods=["POST"])
def api_remove_service():
    """
    Removes the provided service from the user's approved services
    Params:
    - service_id: the service ID of the service to remove
    """
    success, message, user = authenticate_user(request)
    if success == False: return { "message" : message }, 400

    service_id_str = request.args.get("service_id", type=str)

    if service_id_str is None:
        return { "message" : "No service ID provided" }, 400

    try:
        service_id = ObjectId(service_id_str)
    except InvalidId:
        return { "message" : "Service ID is invalid" }, 400

    user_new = mongo.db.users.find_one_and_update(
        {"_id" : user["_id"] },
        { "$pull" : { "approved_services" : { "service_id" : service_id } } },
        return_document=ReturnDocument.AFTER
    )

    if user_new is None:
        return { "message" : "Could not remove service from user" }, 500

    return { "message" : "Success" }, 200

    